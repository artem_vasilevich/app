<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/**
 * @var $prices $_params_['prices] */
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$year = date('Y', $model->created_at);
$month = date('m', $model->created_at) - 1;
$day = date('d', $model->created_at);
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'url:url',
            'created_at:datetime',
            'is_active:boolean',
        ],
    ]) ?>

    <?= Highcharts::widget([
        'scripts' => [
            'modules/exporting',
            'themes/grid-light',
        ],
        'options' => [
            'title' => [
                'text' => 'Combination chart',
            ],
            'xAxis' => [
                'type' => 'datetime',

            ],
            'yAxis' => [
                'title' => [
                    'text' => 'price'
                ],
            ],
            'plotOptions' => [
                'area' => [
                    'fillColor' => [
                        'linearGradient' => [
                            'x1' => 0,
                            'y1' => 0,
                            'x2' => 0,
                            'y2' => 1,
                        ],
                        'stops' => [
                            [0, new JsExpression('Highcharts.getOptions().colors[0]')],
                            [1, new JsExpression('Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get(\'rgba\')')],
                        ],
                    ],
                    'marker' => [
                        'radius' => 2,
                    ],
                    'lineWidth' => 1,
                    'states' => [
                        'hover' => [
                            'lineWidth' => 1,
                        ],
                    ],
                    'threshold' => null,
                ],
            ],
            'legend' => [
                'enabled' => false
            ],
            'series' => [
                [
                    'type' => 'area',
                    'name' => 'price',
                    'data' => $prices,
                    'pointStart' => new JsExpression("Date.UTC($year, $month, $day)"),
                    'pointInterval' => 1 * 3600 * 1000
                ],
            ],
        ],
    ]);
    ?>

</div>
