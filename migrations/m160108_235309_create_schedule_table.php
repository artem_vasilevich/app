<?php

use yii\db\Schema;
use yii\db\Migration;

class m160108_235309_create_schedule_table extends Migration
{
    public function up()
    {
        $this->createTable('schedule',[
            'nothing' => $this->text()
        ]);
    }

    public function down()
    {
        $this->dropTable('schedule');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
