<?php

use yii\db\Schema;
use yii\db\Migration;

class m160108_235230_create_prices_table extends Migration
{
    public function up()
    {
        $this->createTable('prices',[
            'product_id' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-product_id','prices','product_id','products','id','CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk-product_id','prices');
        $this->dropTable('prices');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
