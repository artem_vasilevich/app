<?php

use yii\db\Schema;
use yii\db\Migration;

class m160108_235151_create_products_table extends Migration
{
    public function up()
    {
        $this->createTable('products',[
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'url' => $this->string(255)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer(),
            'is_active' => $this->boolean()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('products');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
