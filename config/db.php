<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=stat',
    'username' => 'homestead',
    'password' => 'secret',
    'charset' => 'utf8',
];
