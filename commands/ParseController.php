<?php

namespace app\commands;

use app\models\Products;
use keltstr\simplehtmldom\SimpleHTMLDom;
use yii\console\Controller;
use app\models\Prices;
use Yii;

class ParseController extends Controller
{
    public function actionIndex()
    {
        if ($products = Products::findAll(['is_active' => true])) {

            foreach ($products as $product) {

                $headers = get_headers($product->url);

                if ($headers[0] === 'HTTP/1.1 200 OK') {

                    $html = SimpleHTMLDom::file_get_html($product->url);

                    $price = $html->find('div[class=b-offers-desc__info-sub]', 0)->first_child()->innertext();
                    $price = explode(' – ', $price);
                    $lowerPrice = str_replace('&nbsp;', '', $price[0]);

                    $prices = new Prices();
                    $prices->price = $lowerPrice;
                    $prices->link('product', $product);
                    if ($prices->save()) {
                        Yii::info('Successful parsed and added');
                    }
                } else {
                    Yii::error('Request error. Headers: ' . $headers);
                }
            }
        }
    }
}