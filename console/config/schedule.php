<?php
/**
 * @var \omnilight\scheduling\Schedule $schedule
 */

// Place here all of your cron jobs

// This command will execute ls command every five minutes
$schedule->exec('ls')->everyMinute();

// This command will execute migration command of your application every hour
$schedule->command('hello')->everyNMinutes(1);
$schedule->command('parse')->everyMinute();
//$schedule->command('parse')->everyMinute();
// This command will call callback function every day at 10:00
/*$schedule->call(function(\yii\console\Application $app) {
    // Some code here...
})->dailyAt('10:00');*/